﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AppMetrica.Portable
{
    public class Query
    {
        public List<string> Sort { get; set; }

        public List<string> Metrics { get; set; }

        public List<string> Dimensions { get; set; }

        public string Ids { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("end_date")]
        public DateTime EndDate { get; set; }
    }
}