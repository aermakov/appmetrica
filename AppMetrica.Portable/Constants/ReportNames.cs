﻿namespace AppMetrica.Portable.Constants
{
    public class ReportNames
    {
        public const string USERS = "Активные пользователи";
        public const string SESSIONS = "Сессии";
        public const string SESSIONS_TIME_INTERVAL = "Длительность сессий";
        public const string APP_VERSIONS = "Версии приложения";
        public const string EVENTS = "События";
        public const string COUNTRIES = "Страны";
        public const string CITIES = "Города";
        public const string OPERATING_SYSTEMS = "Платформа";
        public const string OPERATING_SYSTEM_VERSIONS = "Версии платформы";
        public const string MOBILE_DEVICE_BRANDINGS = "Производители";
        public const string MOBILE_DEVICE_MODELS = "Устройства";
        public const string DEVICE_TYPES = "Тип устройств";
        public const string SCREEN_RESOLUTIONS = "Экраны";
        public const string LOCALES = "Локали";
        public const string OPERATORS = "Операторы";
        public const string CONNECTION_TYPES = "Типы соединений";
        public const string NETWORK_TYPES = "Протоколы";
        public const string CRASHES = "Крэши по дням";
        public const string CRASH_DUMPS = "Крэш логи";
    }
}