﻿namespace AppMetrica.Portable.Constants
{
    public class ReportGroupNames
    {
        public const string USERS = "Пользователи";
        public const string APPLICATION = "Приложение";
        public const string GEOGRAPHY = "География";
        public const string DEVICES = "Устройства";
        public const string NETWORK = "Сеть";
        public const string ERRORS = "Ошибки";
    }
}