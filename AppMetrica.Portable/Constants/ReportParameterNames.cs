﻿namespace AppMetrica.Portable.Constants
{
    public class ReportParameterNames
    {
        public const string START_DATE = "StartDate";
        public const string END_DATE = "EndDate";
        public const string GROUP_BY = "GroupBy";
        public const string APPLICATION_IDS = "ApplicationIds";
        public const string FILTERS = "Filters";
    }
}