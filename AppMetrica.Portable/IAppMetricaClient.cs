﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppMetrica.Portable.Auth;
using AppMetrica.Portable.Enums;

namespace AppMetrica.Portable
{
    public interface IAppMetricaClient
    {
        bool IsAuthorized { get; }

        IAuthorizer Authorizer { get; set; }

        Task AuthorizeAsync();

        Task DeauthorizeAsync();

        Task<List<Application>> GetApplicationsAsync();

        Task<Report> GetReportAsync(
            string ids,
            DateTime startDate,
            DateTime endDate,
            string filters,
            MetricName metricName,
            DimensionName dimensionName);
    }
}