﻿namespace AppMetrica.Portable
{
    public class ReportParameter
    {
        public string Name { get; set; }

        public object Value { get; set; }
    }
}