﻿using System;

namespace AppMetrica.Portable.Extensions
{
    public static class DateTimeExtensions
    {
        private const int SECOND = 1;
        private const int MINUTE = 60*SECOND;
        private const int HOUR = 60*MINUTE;
        private const int DAY = 24*HOUR;
        private const int MONTH = 30*DAY;

        public static string ToTimeString(this TimeSpan ts, out string timeSuffix)
        {
            double totalSeconds = Math.Abs(ts.TotalSeconds);
            if (totalSeconds < 1*MINUTE)
            {
                timeSuffix = ts.Seconds == 1 ? "секунда" : "секунд";
                return ts.Seconds.ToString();
            }

            if (totalSeconds < 45*MINUTE)
            {
                timeSuffix = Plural(ts.Minutes, new[] {"минуту", "минуты", "минут"});
                return ts.Minutes.ToString();
            }

            if (totalSeconds < 24*HOUR)
            {
                timeSuffix = Plural(ts.Hours, new[] {"час", "часа", "часов"});
                return ts.Hours.ToString();
            }

            if (totalSeconds < 30*DAY)
            {
                timeSuffix = Plural(ts.Days, new[] {"день", "дня", "дней"});
                return ts.Days.ToString();
            }

            if (totalSeconds < 12*MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double) ts.Days/30));
                if (months <= 1)
                {
                    timeSuffix = "месяц";
                    return "1";
                }

                timeSuffix = Plural(months, new[] {"месяц", "месяца", "месяцев"});
                return months.ToString();
            }

            int years = Convert.ToInt32(Math.Floor((double) ts.Days/365));
            if (years <= 1)
            {
                timeSuffix = "год";
                return "1";
            }

            timeSuffix = Plural(years, new[] {"год", "года", "лет"});
            return years.ToString();
        }

        public static int ToUnixTimestamp(this DateTime dateTime)
        {
            return (int) (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }

        private static string Plural(int n, string[] forms)
        {
            int plural = (n%10 == 1 && n%100 != 11 ? 0 : n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20) ? 1 : 2);
            return forms.Length > plural ? forms[plural] : string.Empty;
        }
    }
}