﻿namespace AppMetrica.Portable
{
    public class Application
    {
        #region Properties

        public string TimeZone { get; set; }

        public uint Uid { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public uint Id { get; set; }

        public string Platform { get; set; }

        public int Enabled { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}