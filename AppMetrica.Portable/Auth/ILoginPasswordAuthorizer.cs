﻿namespace AppMetrica.Portable.Auth
{
    public interface ILoginPasswordAuthorizer : IAuthorizer
    {
        string Login { get; set; }
        string Password { get; set; }
    }
}