﻿using System.Net.Http;
using System.Threading.Tasks;

namespace AppMetrica.Portable.Auth
{
    public interface IAuthorizer
    {
        bool IsAuthorized { get; }

        Task AuthorizeAsync();

        Task AuthorizeAsync(HttpClient httpClient);

        Task DeauthorizeAsync();

        Task DeauthorizeAsync(HttpClient httpClient);
    }
}