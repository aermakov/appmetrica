﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AppMetrica.Portable.Net;

namespace AppMetrica.Portable.Auth
{
    public class LoginPasswordAuthorizer : ILoginPasswordAuthorizer, IDisposable
    {
        #region Fields

        private const string YANDEX_UID_TOKEN = "\"yandexuid\"";

        private readonly HttpClient _httpClient;

        #endregion

        #region Ctors

        public LoginPasswordAuthorizer()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("User-Agent", AppMetricaClient.USER_AGENT);
        }

        #endregion

        #region Properties

        public string YandexUid { get; private set; }
        public string Login { get; set; }

        public string Password { get; set; }

        public bool IsAuthorized { get; private set; }

        #endregion

        #region Methods

        public async Task AuthorizeAsync()
        {
            await AuthorizeAsync(_httpClient);
        }

        public async Task AuthorizeAsync(HttpClient httpClient)
        {
            await httpClient.GetStringAsync(AppMetricaClient.APP_METRICA_URL);

            HttpContent requestContent =
                new FormUrlEncodedContent(new Dictionary<string, string>
                                          {
                                              {"login", Login},
                                              {"passwd", Password},
                                              {"retpath", AppMetricaClient.APP_METRICA_URL},
                                          });

            var requestUri =
                new Uri(string.Format("{0}passport?mode=auth&from=&twoweeks=yes", AppMetricaClient.PASSPORT_URL));
            using (var requestMessage = new HttpRequestMessage
                                        {
                                            Content = requestContent,
                                            Method = HttpMethod.Post,
                                            RequestUri = requestUri
                                        })
            {
                using (HttpResponseMessage responseMessage = await httpClient.SendAsync(requestMessage))
                {
                    responseMessage.EnsureSuccessStatusCode();

                    string responseContent = await responseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(responseContent))
                    {
                        IsAuthorized = responseContent.Contains("//passport.yandex.ru/passport?mode=logout");
                        if (IsAuthorized)
                        {
                            InitializeYandexUid(responseContent);
                        }
                    }
                }
            }
        }

        public Task DeauthorizeAsync()
        {
            return DeauthorizeAsync(_httpClient);
        }

        public async Task DeauthorizeAsync(HttpClient httpClient)
        {
            string logoutUrl =
                string.Format("https://passport.yandex.ru/passport?mode=logout&yu={0}&retpath={1}",
                    YandexUid,
                    AppMetricaClient.APP_METRICA_URL);

            string responseContent = await httpClient.GetStringAsync(logoutUrl);
            if (responseContent != null)
            {
                IsAuthorized = responseContent.Contains("//passport.yandex.ru/passport?mode=logout");
            }
        }

        private void InitializeYandexUid(string responseContent)
        {
            if (string.IsNullOrEmpty(responseContent)) return;
            responseContent = WebUtility.HtmlDecode(responseContent);

            int index = responseContent.IndexOf(YANDEX_UID_TOKEN, StringComparison.CurrentCultureIgnoreCase);
            if (index == -1) return;
            index += YANDEX_UID_TOKEN.Length + 2; // :"
            int index2 = responseContent.IndexOf('"', index);
            YandexUid = responseContent.Substring(index, index2 - index);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_httpClient != null)
            {
                _httpClient.Dispose();
            }
        }

        #endregion
    }
}