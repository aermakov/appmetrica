﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppMetrica.Portable.Enums;

namespace AppMetrica.Portable
{
    public class Report
    {
        public Report()
        {
            Query = new Query();
            Rows = new List<List<object>>();
            ColumnHeaders = new List<ColumnHeader>();
        }

        public List<ColumnHeader> ColumnHeaders { get; set; }

        public List<List<object>> Rows { get; set; }

        public Query Query { get; set; }

        public int ItemsPerPage { get; set; }

        public int TotalResults { get; set; }

        public DimensionName DimensionName
        {
            get
            {
                string value = DimensionColumnHeader.Name.Replace("ym:m:", string.Empty);
                return (DimensionName) Enum.Parse(typeof (DimensionName), value, true);
            }
        }

        public MetricName MetricName
        {
            get
            {
                string value = MetricColumnHeader.Name.Replace("ym:m:", string.Empty);
                return (MetricName) Enum.Parse(typeof (MetricName), value, true);
            }
        }

        public ColumnHeader DimensionColumnHeader
        {
            get
            {
                return ColumnHeaders.FirstOrDefault(
                    x => (ColumnType) Enum.Parse(typeof (ColumnType), x.ColumnType, true) == ColumnType.Dimension);
            }
        }

        public ColumnHeader MetricColumnHeader
        {
            get
            {
                return ColumnHeaders.FirstOrDefault(
                    x => (ColumnType) Enum.Parse(typeof (ColumnType), x.ColumnType, true) == ColumnType.Metric);
            }
        }
    }
}