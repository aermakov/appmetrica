﻿using System.Collections.Generic;

namespace AppMetrica.Portable
{
    public class ReportParameterCollection : List<ReportParameter>
    {
        public void Add(string name, object value)
        {
            var reportParameter = new ReportParameter {Name = name, Value = value};
            Add(reportParameter);
        }
    }
}