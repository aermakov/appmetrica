﻿using System;
using AppMetrica.Portable.Enums;

namespace AppMetrica.Portable
{
    public class ColumnHeader
    {
        #region Properties

        public string DataType { get; set; }

        public string ColumnType { get; set; }

        public string Name { get; set; }

        public bool IsDate
        {
            get { return (DataType) Enum.Parse(typeof (DataType), DataType, true) == Enums.DataType.Date; }
        }

        public bool IsInteger
        {
            get { return (DataType) Enum.Parse(typeof (DataType), DataType, true) == Enums.DataType.Integer; }
        }

        public bool IsString
        {
            get { return (DataType) Enum.Parse(typeof (DataType), DataType, true) == Enums.DataType.String; }
        }

        public bool IsObject
        {
            get { return (DataType) Enum.Parse(typeof (DataType), DataType, true) == Enums.DataType.Object; }
        }

        #endregion
    }
}