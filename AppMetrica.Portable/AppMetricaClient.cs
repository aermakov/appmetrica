﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AppMetrica.Portable.Auth;
using AppMetrica.Portable.Enums;
using AppMetrica.Portable.Net;
using Newtonsoft.Json.Linq;

namespace AppMetrica.Portable
{
    public class AppMetricaClient : IAppMetricaClient, IDisposable
    {
        #region Fields

        public const string PASSPORT_URL = "http://passport.yandex.ru/";

        public const string APP_METRICA_URL = "http://appmetrika.yandex.ru/";

        public const string USER_AGENT =
            "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";

        private const string REPORTS_URL =
            "http://appmetrika.yandex.ru/ajax/i-reports/";

        private const string APPLICATIONS_URL =
            "http://appmetrika.yandex.ru/ajax/i-applications/";

        private readonly HttpClient _httpClient;

        #endregion

        #region Ctors

        public AppMetricaClient(IAuthorizer authorizer)
        {
            Authorizer = authorizer;

            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
        }

        #endregion

        #region Properties

        public bool IsAuthorized
        {
            get { return Authorizer.IsAuthorized; }
        }

        public IAuthorizer Authorizer { get; set; }

        #endregion

        #region Methods

        public async Task AuthorizeAsync()
        {
            await Authorizer.AuthorizeAsync(_httpClient);
        }

        public async Task<List<Application>> GetApplicationsAsync()
        {
            var applications = new List<Application>();
            using (var requestMessage =
                new HttpRequestMessage(HttpMethod.Get, string.Format("{0}get?resource=applications", APPLICATIONS_URL)))
            {
                using (HttpResponseMessage responseMessage = await _httpClient.SendAsync(requestMessage))
                {
                    if (!responseMessage.IsSuccessStatusCode) return applications;
                    string responseContent = await responseMessage.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(responseContent))
                    {
                        JObject jObject = JObject.Parse(responseContent);
                        applications = jObject.Value<JArray>("applications").ToObject<List<Application>>();
                    }
                }
            }
            return applications;
        }

        public async Task DeauthorizeAsync()
        {
            await Authorizer.DeauthorizeAsync(_httpClient);
        }

        public async Task<Report> GetReportAsync(
            string ids,
            DateTime startDate,
            DateTime endDate,
            string filters,
            MetricName metricName,
            DimensionName dimensionName)
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ids", ids);
            parameters.Add("start-date", startDate.ToString("yyyy-MM-dd"));
            parameters.Add("end-date", endDate.ToString("yyyy-MM-dd"));
            parameters.Add("filters", filters);
            parameters.Add("metrics", string.Format("ym:m:{0}", ToLower(metricName.ToString())));
            parameters.Add("dimensions", string.Format("ym:m:{0}", ToLower(dimensionName.ToString())));
            parameters.Add("debug", "json");
            parameters.Add("lang", "ru");

            var uriBuilder = new UriBuilder(new Uri(REPORTS_URL + "get"));
            string queryString =
                WebUtility.BuildQueryString(new Dictionary<string, string>
                                            {
                                                {"params", JObject.FromObject(parameters).ToString()},
                                                {"resource", "reports"}
                                            });
            uriBuilder.Query = queryString;

            Report report = null;
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, uriBuilder.Uri))
            {
                using (HttpResponseMessage responseMessage = await _httpClient.SendAsync(requestMessage))
                {
                    if (!responseMessage.IsSuccessStatusCode) return report;
                    string responseContent = await responseMessage.Content.ReadAsStringAsync();
                    if (responseContent != null)
                    {
                        report = JObject.Parse(responseContent).ToObject<Report>();
                    }
                }
            }

            return report;
        }

        private string ToLower(string s)
        {
            return char.ToLowerInvariant(s[0]) + s.Substring(1);
        }

        #region IDisposable members

        public void Dispose()
        {
            _httpClient.Dispose();

            if (Authorizer is IDisposable)
            {
                ((IDisposable) Authorizer).Dispose();
            }
        }

        #endregion

        #endregion
    }
}