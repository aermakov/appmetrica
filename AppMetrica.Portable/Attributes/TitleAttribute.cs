﻿using System;

namespace AppMetrica.Portable.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TitleAttribute : Attribute
    {
        public TitleAttribute()
        {
        }

        public TitleAttribute(string title)
        {
            Title = title;
        }

        public string Title { get; set; }
    }
}