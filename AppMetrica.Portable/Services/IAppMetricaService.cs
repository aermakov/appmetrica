﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppMetrica.Portable.Enums;

namespace AppMetrica.Portable.Services
{
    public interface IAppMetricaService
    {
        Task<List<Application>> GetApplicationsAsync();

        Task<Report> GetReportAsync(
            string ids,
            DateTime startDate,
            DateTime endDate,
            string filters,
            MetricName metricName,
            DimensionName dimensionName);

        Task<Report> GetUsersReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetSessionsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetSessionsTimeIntervalReportAsync(ReportParameterCollection reportParameters);

        Task<Report> GetAppVersionsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetEventsReportAsync(ReportParameterCollection reportParameters);

        Task<Report> GetCountriesReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetCitiesReportAsync(ReportParameterCollection reportParameters);

        Task<Report> GetOperatingSystemsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetOperatingSystemVersionsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetMobileDeviceBrandingsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetMobileDeviceModelsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetDeviceTypesReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetScreenResolutionsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetLocalesReportAsync(ReportParameterCollection reportParameters);

        Task<Report> GetOperatorsReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetConnectionTypesReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetNetworkTypesReportAsync(ReportParameterCollection reportParameters);

        Task<Report> GetCrashesReportAsync(ReportParameterCollection reportParameters);
        Task<Report> GetCrashDumpsReportAsync(ReportParameterCollection reportParameters);
    }
}