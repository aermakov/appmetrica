﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppMetrica.Portable.Constants;
using AppMetrica.Portable.Enums;

namespace AppMetrica.Portable.Services
{
    public class AppMetricaService : IAppMetricaService
    {
        private readonly IAppMetricaClient _appMetricaClient;

        public AppMetricaService(IAppMetricaClient appMetricaClient)
        {
            _appMetricaClient = appMetricaClient;
        }

        public Task<List<Application>> GetApplicationsAsync()
        {
            return _appMetricaClient.GetApplicationsAsync();
        }

        public Task<Report> GetReportAsync(
            string ids,
            DateTime startDate,
            DateTime endDate,
            string filters,
            MetricName metricName,
            DimensionName dimensionName)
        {
            return _appMetricaClient.GetReportAsync(ids, startDate, endDate, filters, metricName, dimensionName);
        }

        public Task<Report> GetUsersReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.Date);
        }

        public Task<Report> GetSessionsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Sessions, DimensionName.Date);
        }

        public Task<Report> GetSessionsTimeIntervalReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Sessions, DimensionName.SessionTimeIntervalObj);
        }

        public Task<Report> GetAppVersionsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.AppVersion);
        }

        public Task<Report> GetEventsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.TotalEvents, DimensionName.EventLabel);
        }

        public Task<Report> GetCountriesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.RegionCountryObj);
        }

        public Task<Report> GetCitiesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.RegionCityObj);
        }

        public Task<Report> GetOperatingSystemsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.OperatingSystem);
        }

        public Task<Report> GetOperatingSystemVersionsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.OperatingSystemVersion);
        }

        public Task<Report> GetMobileDeviceBrandingsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.MobileDeviceBranding);
        }

        public Task<Report> GetMobileDeviceModelsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.MobileDeviceModel);
        }

        public Task<Report> GetDeviceTypesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.DeviceType);
        }

        public Task<Report> GetScreenResolutionsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.ScreenResolutionObj);
        }

        public Task<Report> GetLocalesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.Locale);
        }

        public Task<Report> GetOperatorsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.Operator);
        }

        public Task<Report> GetConnectionTypesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.ConnectionType);
        }

        public Task<Report> GetNetworkTypesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Users, DimensionName.NetworkType);
        }

        public Task<Report> GetCrashesReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Crashes, DimensionName.Date);
        }

        public Task<Report> GetCrashDumpsReportAsync(ReportParameterCollection reportParameters)
        {
            return GetReportAsync(reportParameters, MetricName.Crashes, DimensionName.CrashDumpObj);
        }

        private Task<Report> GetReportAsync(
            IList<ReportParameter> reportParameters,
            MetricName metricName,
            DimensionName dimensionName)
        {
            DateTime startDate = GetStartDate(reportParameters);
            DateTime endDate = GetEndDate(reportParameters);
            GroupBy groupBy = GetGroupBy(reportParameters);
            string ids = GetApplicationIds(reportParameters);
            string filters = GetFilters(reportParameters);

            return _appMetricaClient.GetReportAsync(ids, startDate, endDate, filters, metricName, dimensionName);
        }

        private DateTime GetStartDate(IEnumerable<ReportParameter> reportParameters)
        {
            DateTime startDate = DateTime.Today.AddDays(-7);

            ReportParameter reportParameter =
                reportParameters.SingleOrDefault(x => x.Name == ReportParameterNames.START_DATE);
            if (reportParameter != null && reportParameter.Value is DateTime)
            {
                startDate = (DateTime) reportParameter.Value;
            }
            return startDate;
        }

        private string GetApplicationIds(IEnumerable<ReportParameter> reportParameters)
        {
            string applicationIds = string.Empty;

            ReportParameter reportParameter =
                reportParameters.SingleOrDefault(x => x.Name == ReportParameterNames.APPLICATION_IDS);
            if (reportParameter != null && reportParameter.Value is string)
            {
                applicationIds = (string) reportParameter.Value;
            }
            return applicationIds;
        }

        private string GetFilters(IEnumerable<ReportParameter> reportParameters)
        {
            string filters = string.Empty;

            ReportParameter reportParameter =
                reportParameters.SingleOrDefault(x => x.Name == ReportParameterNames.FILTERS);
            if (reportParameter != null && reportParameter.Value is string)
            {
                filters = (string) reportParameter.Value;
            }
            return filters;
        }

        private DateTime GetEndDate(IEnumerable<ReportParameter> reportParameters)
        {
            DateTime endDate = DateTime.Today;

            ReportParameter reportParameter =
                reportParameters.SingleOrDefault(x => x.Name == ReportParameterNames.END_DATE);
            if (reportParameter != null && reportParameter.Value is DateTime)
            {
                endDate = (DateTime) reportParameter.Value;
            }
            return endDate;
        }

        private GroupBy GetGroupBy(IEnumerable<ReportParameter> reportParameters)
        {
            var groupBy = GroupBy.Date;

            ReportParameter reportParameter =
                reportParameters.SingleOrDefault(x => x.Name == ReportParameterNames.GROUP_BY);
            if (reportParameter != null && reportParameter.Value is GroupBy)
            {
                groupBy = (GroupBy) reportParameter.Value;
            }
            return groupBy;
        }
    }
}