﻿namespace AppMetrica.Portable.Enums
{
    public enum GroupBy
    {
        Date,
        Week,
        Month
    }
}