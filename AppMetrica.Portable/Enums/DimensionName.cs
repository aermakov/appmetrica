﻿using AppMetrica.Portable.Attributes;

namespace AppMetrica.Portable.Enums
{
    /// <summary>
    ///     Наименования измерений
    /// </summary>
    public enum DimensionName
    {
        [Title("Дата")] Date,
        [Title("Версия приложения")] AppVersion,
        [Title("Длительность сессии")] SessionTimeIntervalObj,
        [Title("Название события")] EventLabel,
        [Title("Страна")] RegionCountryObj,
        [Title("Город")] RegionCityObj,
        [Title("Платформа")] OperatingSystem,
        [Title("Версия платформы")] OperatingSystemVersion,
        [Title("Производители")] MobileDeviceBranding,
        [Title("Устройства")] MobileDeviceModel,
        [Title("Типы устройств")] DeviceType,
        [Title("Разрешения")] ScreenResolutionObj,
        [Title("Локали")] Locale,
        [Title("Операторы")] Operator,
        [Title("Типы соединений")] ConnectionType,
        [Title("Сети")] NetworkType,
        [Title("Крэш логи")] CrashDumpObj
    }
}