﻿namespace AppMetrica.Portable.Enums
{
    public enum ColumnType
    {
        Dimension,
        Metric
    }
}