﻿using AppMetrica.Portable.Attributes;

namespace AppMetrica.Portable.Enums
{
    public enum Period
    {
        [Title("Неделя")] Week,
        [Title("Месяц")] Month,
        [Title("Квартал")] Quarter,
        [Title("Год")] Year
    }
}