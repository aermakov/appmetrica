﻿namespace AppMetrica.Portable.Enums
{
    public enum DataType
    {
        Object,
        String,
        Date,
        Integer
    }
}