﻿using AppMetrica.Portable.Attributes;

namespace AppMetrica.Portable.Enums
{
    /// <summary>
    ///     Наименования метрик
    /// </summary>
    public enum MetricName
    {
        [Title("Пользователи")] Users,
        [Title("Сессии")] Sessions,
        [Title("События")] TotalEvents,
        [Title("Ошибки")] Crashes
    }
}