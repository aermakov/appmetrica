﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using AppMetrica.Models;
using AppMetrica.Portable;
using AppMetrica.Portable.Constants;
using AppMetrica.Portable.Services;
using AppMetrica.Services;
using Caliburn.Micro;
using Application = AppMetrica.Portable.Application;

namespace AppMetrica.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAppMetricaClient _appMetricaClient;
        private readonly IAppMetricaService _appMetricaService;
        private readonly ISettingsService _settingsService;
        private BindableCollection<ApplicationModel> _applications;
        private Report _sessionsReport;
        private Report _usersReport;

        #endregion

        #region Ctors

        public MainViewModel(INavigationService navigationService,
            IAppMetricaService appMetricaService,
            IAppMetricaClient appMetricaClient,
            ISettingsService settingsService)
            : base(navigationService)
        {
            _appMetricaService = appMetricaService;
            _appMetricaClient = appMetricaClient;
            _settingsService = settingsService;

            Applications = new BindableCollection<ApplicationModel>();
        }

        #endregion

        #region Properties

        public BindableCollection<ApplicationModel> Applications
        {
            get { return _applications; }
            set
            {
                if (Equals(value, _applications)) return;
                _applications = value;
                NotifyOfPropertyChange(() => Applications);
            }
        }

        public Report SessionsReport
        {
            get { return _sessionsReport; }
            set
            {
                if (Equals(value, _sessionsReport)) return;
                _sessionsReport = value;
                NotifyOfPropertyChange(() => SessionsReport);
            }
        }

        public Report UsersReport
        {
            get { return _usersReport; }
            set
            {
                if (Equals(value, _usersReport)) return;
                _usersReport = value;
                NotifyOfPropertyChange(() => UsersReport);
            }
        }

        #endregion Properties

        #region Methods

        public async void Deauthorize()
        {
            if (await GetIsNetworkAvailableAsync())
            {
                await _appMetricaClient.DeauthorizeAsync();
            }

            if (!_appMetricaClient.IsAuthorized)
            {
                _settingsService.AutoLogin = false;
                _settingsService.Password = string.Empty;

                NavigationService.UriFor<AuthorizationViewModel>().Navigate();
            }
        }

        public void OnApplicationsTapped(FrameworkElement sender)
        {
            var applicationModel = (ApplicationModel) sender.DataContext;

            NavigationService.UriFor<ApplicationViewModel>()
                .WithParam(x => x.ApplicationId, applicationModel.Id)
                .WithParam(x => x.ApplicationName, applicationModel.Name)
                .Navigate();
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            await LoadApplicationsAsync();

            var reportParameters =
                new ReportParameterCollection
                {
                    {
                        ReportParameterNames.APPLICATION_IDS,
                        string.Join(",", Applications.Select(x => x.Id.ToString(CultureInfo.InvariantCulture)))
                    }
                };

            if (await GetIsNetworkAvailableAsync())
            {
                UsersReport = await _appMetricaService.GetUsersReportAsync(reportParameters);

                SessionsReport = await _appMetricaService.GetSessionsReportAsync(reportParameters);
            }

            RemoveBackEntry();
        }

        private void RemoveBackEntry()
        {
            JournalEntry entry = NavigationService.BackStack.LastOrDefault();
            if (entry == null) return;
            string source = entry.Source.ToString();

            string authenticationViewUriString =
                NavigationService.UriFor<AuthorizationViewModel>().BuildUri().ToString();

            string splashScreenViewUriString =
                NavigationService.UriFor<SplashScreenViewModel>().BuildUri().ToString();

            if (source.EndsWith(authenticationViewUriString) || source.EndsWith(splashScreenViewUriString))
            {
                NavigationService.RemoveBackEntry();
            }
        }

        private async Task LoadApplicationsAsync()
        {
            List<Application> applications;
            if (await GetIsNetworkAvailableAsync())
            {
                applications = await _appMetricaService.GetApplicationsAsync();
            }
            else
            {
                applications = new List<Application>();
            }

            Applications.IsNotifying = false;
            Applications.Clear();
            foreach (Application application in applications)
            {
                Applications.Add(new ApplicationModel(application));
            }
            Applications.IsNotifying = true;
            Applications.Refresh();
        }

        #endregion
    }
}