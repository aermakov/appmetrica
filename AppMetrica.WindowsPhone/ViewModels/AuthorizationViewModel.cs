﻿using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using AppMetrica.Portable;
using AppMetrica.Portable.Auth;
using AppMetrica.Services;
using Caliburn.Micro;

namespace AppMetrica.ViewModels
{
    public class AuthorizationViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAppMetricaClient _appMetricaClient;
        private readonly ISettingsService _settingsService;

        private string _login;
        private string _password;

        private bool _rememberMe;

        #endregion

        #region Ctors

        public AuthorizationViewModel(INavigationService navigationService,
            ISettingsService settingsService,
            IAppMetricaClient appMetricaClient)
            : base(navigationService)
        {
            _settingsService = settingsService;
            _appMetricaClient = appMetricaClient;
        }

        #endregion

        #region Properties

        public string Login
        {
            get { return _login; }
            set
            {
                if (value == _login) return;
                _login = value;
                NotifyOfPropertyChange(() => Login);
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (value == _password) return;
                _password = value;
                NotifyOfPropertyChange(() => Password);
            }
        }

        public bool RememberMe
        {
            get { return _rememberMe; }
            set
            {
                if (value.Equals(_rememberMe)) return;
                _rememberMe = value;
                NotifyOfPropertyChange(() => RememberMe);
            }
        }

        #endregion

        #region Methods

        public async void Authorize()
        {
            IAuthorizer authorizer = _appMetricaClient.Authorizer;
            var loginPasswordAuthorizer = authorizer as ILoginPasswordAuthorizer;
            if (loginPasswordAuthorizer != null)
            {
                loginPasswordAuthorizer.Login = Login;
                loginPasswordAuthorizer.Password = Password;
            }

            if (await GetIsNetworkAvailableAsync())
            {
                await _appMetricaClient.AuthorizeAsync();

                if (_appMetricaClient.IsAuthorized)
                {
                    _settingsService.Login = Login;
                    _settingsService.Password = Password;
                    _settingsService.AutoLogin = true;

                    NavigationService.UriFor<MainViewModel>().Navigate();
                }
                else
                {
                    MessageBox.Show(
                        "Ошибка при авторизации. Проверьте правильность ввода логина и пароля и повторите попытку входа.",
                        AppResources.ApplicationName, MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show(AppResources.NetworkUnavailable, AppResources.ApplicationName, MessageBoxButton.OK);
            }
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            Login = _settingsService.Login;

            if (!string.IsNullOrEmpty(_settingsService.Password) && _settingsService.AutoLogin)
            {
                Password = _settingsService.Password;
            }

            RemoveBackEntry();
        }

        private void RemoveBackEntry()
        {
            JournalEntry entry = NavigationService.BackStack.LastOrDefault();
            if (entry == null) return;
            string source = entry.Source.ToString();

            string mainViewUriString =
                NavigationService.UriFor<MainViewModel>().BuildUri().ToString();

            string splashScreenViewUriString =
                NavigationService.UriFor<SplashScreenViewModel>().BuildUri().ToString();

            if (source.EndsWith(mainViewUriString) || source.EndsWith(splashScreenViewUriString))
            {
                NavigationService.RemoveBackEntry();
            }
        }

        #endregion
    }
}