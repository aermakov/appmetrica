﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AppMetrica.Models;
using AppMetrica.Portable;
using AppMetrica.Portable.Constants;
using AppMetrica.Portable.Enums;
using AppMetrica.Portable.Services;
using AppMetrica.Services;
using Caliburn.Micro;
using Microsoft.Phone.Controls;

namespace AppMetrica.ViewModels
{
    public class ApplicationViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAnalyticalService _analyticalService;
        private readonly IAppMetricaService _appMetricaService;

        private bool _isMonthChecked;
        private bool _isQuarterChecked;
        private bool _isWeekChecked;
        private bool _isYearChecked;

        private DateTime _periodFrom;
        private DateTime _periodTo;
        private Report _report;
        private string _reportName;
        private BindableCollection<ReportModel> _reports;

        private int _selectedPivotItemIndex;
        private ReportModel _selectedReport;
        private int _xAxisInterval;

        #endregion

        #region Ctors

        public ApplicationViewModel(INavigationService navigationService,
            IAppMetricaService appMetricaService,
            IAnalyticalService analyticalService)
            : base(navigationService)
        {
            _appMetricaService = appMetricaService;
            _analyticalService = analyticalService;

            _reports = new BindableCollection<ReportModel>();

            XAxisInterval = 1;
            PeriodFrom = DateTime.Today.AddDays(-7);
            PeriodTo = DateTime.Today;
            IsWeekChecked = true;
        }

        #endregion

        #region Properties

        public string ApplicationName { get; set; }

        public uint ApplicationId { get; set; }

        public BindableCollection<ReportModel> Reports
        {
            get { return _reports; }
            set
            {
                if (Equals(value, _reports)) return;
                _reports = value;
                NotifyOfPropertyChange(() => Reports);
            }
        }

        public string ReportName
        {
            get { return _reportName; }
            set
            {
                if (value == _reportName) return;
                _reportName = value;
                NotifyOfPropertyChange(() => ReportName);
            }
        }

        public int XAxisInterval
        {
            get { return _xAxisInterval; }
            set
            {
                if (value == _xAxisInterval) return;
                _xAxisInterval = value;
                NotifyOfPropertyChange(() => XAxisInterval);
            }
        }

        public int SelectedPivotItemIndex
        {
            get { return _selectedPivotItemIndex; }
            set
            {
                if (value == _selectedPivotItemIndex) return;
                _selectedPivotItemIndex = value;
                NotifyOfPropertyChange(() => SelectedPivotItemIndex);
            }
        }

        public DateTime PeriodFrom
        {
            get { return _periodFrom; }
            set
            {
                if (value.Equals(_periodFrom)) return;
                _periodFrom = value;
                NotifyOfPropertyChange(() => PeriodFrom);
            }
        }

        public DateTime PeriodTo
        {
            get { return _periodTo; }
            set
            {
                if (value.Equals(_periodTo)) return;
                _periodTo = value;
                NotifyOfPropertyChange(() => PeriodTo);
            }
        }

        public bool IsWeekChecked
        {
            get { return _isWeekChecked; }
            set
            {
                if (value.Equals(_isWeekChecked)) return;
                _isWeekChecked = value;
                NotifyOfPropertyChange(() => IsWeekChecked);
            }
        }

        public bool IsMonthChecked
        {
            get { return _isMonthChecked; }
            set
            {
                if (value.Equals(_isMonthChecked)) return;
                _isMonthChecked = value;
                NotifyOfPropertyChange(() => IsMonthChecked);
            }
        }

        public bool IsQuarterChecked
        {
            get { return _isQuarterChecked; }
            set
            {
                if (value.Equals(_isQuarterChecked)) return;
                _isQuarterChecked = value;
                NotifyOfPropertyChange(() => IsQuarterChecked);
            }
        }

        public bool IsYearChecked
        {
            get { return _isYearChecked; }
            set
            {
                if (value.Equals(_isYearChecked)) return;
                _isYearChecked = value;
                NotifyOfPropertyChange(() => IsYearChecked);
            }
        }

        public Report Report
        {
            get { return _report; }
            set
            {
                if (Equals(value, _report)) return;
                _report = value;
                NotifyOfPropertyChange(() => Report);
            }
        }

        public ReportModel SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                if (Equals(value, _selectedReport)) return;
                _selectedReport = value;
                NotifyOfPropertyChange(() => SelectedReport);
                if (_selectedReport != null)
                {
                    ReportName = _selectedReport.Name.ToLowerInvariant();
                }
            }
        }

        #endregion

        #region Methods

        public async void OnPeriodFromValueChanged()
        {
            IsWeekChecked = IsMonthChecked = IsQuarterChecked = IsYearChecked = false;

            await UpdateReportAsync();
        }

        public async void OnPeriodToValueChanged()
        {
            IsWeekChecked = IsMonthChecked = IsQuarterChecked = IsYearChecked = false;

            await UpdateReportAsync();
        }

        public void Week()
        {
            PeriodFrom = DateTime.Today.AddDays(-7);
            PeriodTo = DateTime.Today;

            IsWeekChecked = true;
            XAxisInterval = 1;
        }

        public void Month()
        {
            PeriodFrom = DateTime.Today.AddMonths(-1);
            PeriodTo = DateTime.Today;

            IsMonthChecked = true;
            XAxisInterval = 5;
        }

        public void Quarter()
        {
            PeriodFrom = DateTime.Today.AddMonths(-3);
            PeriodTo = DateTime.Today;

            IsQuarterChecked = true;
            XAxisInterval = 10;
        }

        public void Year()
        {
            PeriodFrom = DateTime.Today.AddYears(-1);
            PeriodTo = DateTime.Today;

            IsYearChecked = true;
            XAxisInterval = 40;
        }

        private int GetPivotItemIndex(string header)
        {
            int index = -1;
            var pivot = (Pivot) ((FrameworkElement) GetView()).FindName("Pivot");
            for (int i = 0; i < pivot.Items.Count && index == -1; i++)
            {
                var item = (PivotItem) pivot.Items[i];
                if (Equals(item.Header, header))
                {
                    index = i;
                }
            }
            return index;
        }

        public async void OnReportsSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count <= 0) return;

            var reportModel = (ReportModel) e.AddedItems[0];

            await UpdateReportAsync(reportModel);

            SelectedPivotItemIndex = GetPivotItemIndex(ReportName.ToLowerInvariant());
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            InitializeReports();

            SelectedReport = Reports[0];

            await UpdateReportAsync(SelectedReport);
        }

        private async Task UpdateReportAsync()
        {
            await UpdateReportAsync(SelectedReport);
        }

        private async Task UpdateReportAsync(ReportModel reportModel)
        {
            DateTime startDate = PeriodFrom;
            DateTime endDate = PeriodTo;
            string ids = ApplicationId.ToString(CultureInfo.InvariantCulture);
            string filters = string.Empty;
            MetricName metricName = reportModel.MetricName;
            DimensionName dimensionName = reportModel.DimensionName;

            if (await GetIsNetworkAvailableAsync())
            {
                Report report = await _appMetricaService.GetReportAsync(
                    ids,
                    startDate,
                    endDate,
                    filters,
                    metricName,
                    dimensionName);

                if (report != null)
                {
                    UpdateReport(report, reportModel.Name);
                }
            }
        }

        private void UpdateReport(Report report, string reportName)
        {
            string eventName = string.Format("{0} Report Viewed", reportName);

            _analyticalService.ReportEvent(eventName);

            ReportName = reportName.ToLowerInvariant();
            Report = report;
        }

        private void InitializeReports()
        {
            #region Users

            Reports.Add(new ReportModel(ReportGroupNames.USERS, ReportNames.USERS,
                MetricName.Users, DimensionName.Date));

            Reports.Add(new ReportModel(ReportGroupNames.USERS, ReportNames.SESSIONS,
                MetricName.Sessions, DimensionName.Date));

            Reports.Add(new ReportModel(ReportGroupNames.USERS, ReportNames.SESSIONS_TIME_INTERVAL,
                MetricName.Sessions, DimensionName.SessionTimeIntervalObj));

            #endregion

            #region Application

            Reports.Add(new ReportModel(ReportGroupNames.APPLICATION, ReportNames.APP_VERSIONS,
                MetricName.Users, DimensionName.AppVersion));

            Reports.Add(new ReportModel(ReportGroupNames.APPLICATION, ReportNames.EVENTS,
                MetricName.TotalEvents, DimensionName.EventLabel));

            #endregion

            #region Geography

            Reports.Add(new ReportModel(ReportGroupNames.GEOGRAPHY, ReportNames.COUNTRIES,
                MetricName.Users, DimensionName.RegionCountryObj));

            Reports.Add(new ReportModel(ReportGroupNames.GEOGRAPHY, ReportNames.CITIES,
                MetricName.Users, DimensionName.RegionCityObj));

            #endregion

            #region Devices

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.OPERATING_SYSTEMS,
                MetricName.Users, DimensionName.OperatingSystem));

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.OPERATING_SYSTEM_VERSIONS,
                MetricName.Users, DimensionName.OperatingSystemVersion));

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.MOBILE_DEVICE_BRANDINGS,
                MetricName.Users, DimensionName.MobileDeviceBranding));

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.MOBILE_DEVICE_MODELS,
                MetricName.Users, DimensionName.MobileDeviceModel));

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.DEVICE_TYPES,
                MetricName.Users, DimensionName.DeviceType));

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.SCREEN_RESOLUTIONS,
                MetricName.Users, DimensionName.ScreenResolutionObj));

            Reports.Add(new ReportModel(ReportGroupNames.DEVICES, ReportNames.LOCALES,
                MetricName.Users, DimensionName.Locale));

            #endregion

            #region Network

            Reports.Add(new ReportModel(ReportGroupNames.NETWORK, ReportNames.OPERATORS,
                MetricName.Users, DimensionName.Operator));

            Reports.Add(new ReportModel(ReportGroupNames.NETWORK, ReportNames.CONNECTION_TYPES,
                MetricName.Users, DimensionName.ConnectionType));

            Reports.Add(new ReportModel(ReportGroupNames.NETWORK, ReportNames.NETWORK_TYPES,
                MetricName.Users, DimensionName.NetworkType));

            #endregion

            #region Errors

            Reports.Add(new ReportModel(ReportGroupNames.ERRORS, ReportNames.CRASHES,
                MetricName.Crashes, DimensionName.Date));

            Reports.Add(new ReportModel(ReportGroupNames.ERRORS, ReportNames.CRASH_DUMPS,
                MetricName.Crashes, DimensionName.CrashDumpObj));

            #endregion
        }

        #endregion
    }
}