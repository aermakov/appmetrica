﻿using AppMetrica.Portable;
using AppMetrica.Portable.Auth;
using AppMetrica.Services;
using Caliburn.Micro;

namespace AppMetrica.ViewModels
{
    public class SplashScreenViewModel : ViewModelBase
    {
        private readonly IAppMetricaClient _appMetricaClient;
        private readonly ISettingsService _settingsService;

        public SplashScreenViewModel(INavigationService navigationService,
            ISettingsService settingsService,
            IAppMetricaClient appMetricaClient)
            : base(navigationService)
        {
            _settingsService = settingsService;
            _appMetricaClient = appMetricaClient;
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            string login = _settingsService.Login;
            string password = _settingsService.Password;
            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password) && _settingsService.AutoLogin)
            {
                IAuthorizer authorizer = _appMetricaClient.Authorizer;
                var loginPasswordAuthorizer = authorizer as ILoginPasswordAuthorizer;
                if (loginPasswordAuthorizer != null)
                {
                    loginPasswordAuthorizer.Login = login;
                    loginPasswordAuthorizer.Password = password;
                }

                if (await GetIsNetworkAvailableAsync())
                {
                    await _appMetricaClient.AuthorizeAsync();
                }

                if (!_appMetricaClient.IsAuthorized)
                {
                    NavigateToAuthenticationView();
                }
                else
                {
                    NavigateToMainView();
                }
            }
            else
            {
                NavigateToAuthenticationView();
            }
        }

        private void NavigateToAuthenticationView()
        {
            NavigationService.UriFor<AuthorizationViewModel>().Navigate();
        }

        private void NavigateToMainView()
        {
            NavigationService.UriFor<MainViewModel>().Navigate();
        }
    }
}