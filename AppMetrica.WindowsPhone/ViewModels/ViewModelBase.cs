﻿using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;
using AppMetrica.Portable.Net;
using Caliburn.Micro;

namespace AppMetrica.ViewModels
{
    public abstract class ViewModelBase : Screen
    {
        #region Fields

        protected readonly INavigationService NavigationService;

        #endregion

        #region Ctors

        protected ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        #endregion

        #region Properties

        protected Dispatcher Dispatcher
        {
            get { return ((Page) GetView()).Dispatcher; }
        }

        #endregion

        #region Methods

        public void About()
        {
            NavigationService.UriFor<AboutViewModel>().Navigate();
        }

        public void Main()
        {
            NavigationService.UriFor<MainViewModel>().Navigate();
        }

        protected void GoBack()
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        protected async Task<bool> GetIsNetworkAvailableAsync()
        {
            return await WebUtility.GetIsNetworkAvailableAsync();
        }

        #endregion
    }
}