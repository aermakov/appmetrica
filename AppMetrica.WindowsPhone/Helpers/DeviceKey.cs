﻿namespace AppMetrica.Helpers
{
    public enum DeviceKey
    {
        DeviceName,
        DeviceUniqueId,
        DeviceManufacturer,
        ApplicationCurrentMemoryUsage,
        ApplicationPeakMemoryUsage,
        DeviceFirmwareVersion,
        DeviceHardwareVersion,
        DeviceTotalMemory
    }
}