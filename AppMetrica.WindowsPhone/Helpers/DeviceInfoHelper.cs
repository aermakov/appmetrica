﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Phone.Info;

namespace AppMetrica.Helpers
{
    public static class DeviceInfoHelper
    {
        private const int ANIDLength = 32;
        private const int ANIDOffset = 2;

        public static DateTime GetReleaseDate()
        {
            string versionNumber = GetVersionNumber();
            string[] parts = versionNumber.Split('.');
            int year = Convert.ToInt32(parts.Last().Substring(0, 2));
            int dayOfYear = Convert.ToInt32(parts.Last().Substring(2));
            DateTime releaseDate = new DateTime(2000 + year, 1, 1).AddDays(dayOfYear - 1);
            return releaseDate;
        }

        public static string GetVersionNumber()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string[] parts = assembly.FullName.Split(',');
            return parts[1].Split('=')[1];
        }

        public static T GetDeviceInfo<T>(DeviceKey key)
        {
            T result = default(T);
            object deviceInfo;
            if (DeviceExtendedProperties.TryGetValue(key.ToString(), out deviceInfo))
            {
                result = (T) deviceInfo;
            }
            return result;
        }

        public static string GetDeviceId()
        {
            return Convert.ToBase64String(GetDeviceInfo<byte[]>(DeviceKey.DeviceUniqueId));
        }

        public static string GetANID()
        {
            string result = string.Empty;
            object anid;
            if (!UserExtendedProperties.TryGetValue("ANID", out anid)) return result;
            if (anid != null && anid.ToString().Length >= (ANIDLength + ANIDOffset))
            {
                result = anid.ToString().Substring(ANIDOffset, ANIDLength);
            }

            return result;
        }
    }
}