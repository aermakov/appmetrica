﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using AppMetrica.Portable;
using AppMetrica.Portable.Auth;
using AppMetrica.Portable.Services;
using AppMetrica.Services;
using AppMetrica.ViewModels;
using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;
using Microsoft.Phone.Shell;
using Yandex.Metrica;

namespace AppMetrica
{
    public class AppBootstrapper : PhoneBootstrapper
    {
        #region Fields

        private PhoneContainer _container;

        #endregion

        protected override void Configure()
        {
            _container = new PhoneContainer();

            _container.RegisterPhoneServices(RootFrame);

            RegisterSingleton<MainViewModel>();
            RegisterSingleton<AboutViewModel>();
            RegisterSingleton<AuthorizationViewModel>();
            RegisterSingleton<SplashScreenViewModel>();

            _container.PerRequest<ApplicationViewModel>();

            _container.RegisterSingleton(typeof (IAppMetricaClient), null, typeof (AppMetricaClient));
            _container.RegisterSingleton(typeof (IAppMetricaService), null, typeof (AppMetricaService));
            _container.RegisterSingleton(typeof (IAuthorizer), null, typeof (LoginPasswordAuthorizer));
            _container.RegisterSingleton(typeof (IAnalyticalService), null, typeof (YandexMetricaAnalyticalService));
            _container.RegisterSingleton(typeof (ISettingsService), null, typeof (SettingsService));

            AddCustomConventions();
        }

        private static void AddCustomConventions()
        {
            // App Bar Conventions
            ConventionManager.AddElementConvention<BindableAppBarButton>(
                Control.IsEnabledProperty, "DataContext", "Click");

            ConventionManager.AddElementConvention<BindableAppBarMenuItem>(
                Control.IsEnabledProperty, "DataContext", "Click");
        }

        private void RegisterSingleton<T>()
        {
            _container.RegisterSingleton(typeof (T), null, typeof (T));
        }

        protected override void OnLaunch(object sender, LaunchingEventArgs e)
        {
        }

        protected override void OnUnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            var analyticalService = (IAnalyticalService) _container.GetInstance(typeof (IAnalyticalService), null);
            if (analyticalService != null)
            {
                analyticalService.ReportError(e.ExceptionObject);
            }

            if (Debugger.IsAttached)
            {
                Debugger.Break();
                e.Handled = true;
            }
            else
            {
                MessageBox.Show(AppResources.UnexpectedError, "Oops...", MessageBoxButton.OK);
                e.Handled = true;
            }

            base.OnUnhandledException(sender, e);
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override void OnClose(object sender, ClosingEventArgs e)
        {
            Counter.ReportExit();

            base.OnClose(sender, e);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }
}