﻿using Yandex.Metrica;

namespace AppMetrica
{
    public partial class App
    {
        /// <summary>
        ///     Constructor for the Application object.
        /// </summary>
        public App()
        {
            InitializeComponent();

            Counter.Start(Settings.YANDEX_METRICA_API_KEY);
            Counter.StartNewSessionManually();
        }
    }
}