﻿using System;
using Sparrow.Chart;

namespace AppMetrica.Models
{
    public class ChartPointModel
    {
        public ChartPointModel(TimePoint timePoint)
        {
            Time = timePoint.Time;
            Value = timePoint.Value;
        }

        public ChartPointModel(CategoryPoint categoryPoint)
        {
            Category = categoryPoint.Category;
            Value = categoryPoint.Value;
        }

        public DateTime Time { get; set; }

        public string Category { get; set; }

        public double Value { get; set; }
    }
}