﻿using AppMetrica.Portable.Enums;

namespace AppMetrica.Models
{
    public class ReportModel
    {
        #region Ctors

        public ReportModel()
        {
        }

        public ReportModel(string groupName, string name, MetricName metricName, DimensionName dimensionName)
        {
            GroupName = groupName;
            Name = name;
            MetricName = metricName;
            DimensionName = dimensionName;
        }

        #endregion

        #region Properties

        public string GroupName { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public MetricName MetricName { get; set; }

        public DimensionName DimensionName { get; set; }

        #endregion
    }
}