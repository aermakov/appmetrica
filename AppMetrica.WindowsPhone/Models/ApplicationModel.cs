﻿using AppMetrica.Portable;

namespace AppMetrica.Models
{
    public class ApplicationModel
    {
        #region Ctors

        public ApplicationModel(Application application)
        {
            Name = application.Name;
            Id = application.Id;
        }

        #endregion

        #region Properties

        public uint Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}