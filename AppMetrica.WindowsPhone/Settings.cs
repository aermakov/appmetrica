﻿using System;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Security.Cryptography;
using System.Text;

namespace AppMetrica
{
    public class Settings
    {
        #region Fields

        public const uint YANDEX_METRICA_API_KEY = 12982;

        protected readonly IsolatedStorageSettings _settings;

        #endregion

        #region Ctors

        /// <summary>
        ///     Constructor that gets the application settings.
        /// </summary>
        protected internal Settings()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                // Get the settings for this application.
                _settings = IsolatedStorageSettings.ApplicationSettings;
            }
        }

        #endregion

        #region Properties

        public string Login
        {
            get { return GetValueOrDefault<string>("Login", null); }
            set { if (AddOrUpdateValue("Login", value)) Save(); }
        }

        public string Password
        {
            get
            {
                byte[] bytes = GetValueOrDefault("Password", new byte[] {});
                if (bytes.Length > 0)
                {
                    byte[] unencryptedBytes = ProtectedData.Unprotect(bytes, null);
                    return Encoding.UTF8.GetString(unencryptedBytes, 0, unencryptedBytes.Length);
                }
                return string.Empty;
            }
            set
            {
                byte[] encryptedBytes = ProtectedData.Protect(Encoding.UTF8.GetBytes(value), null);
                if (AddOrUpdateValue("Password", encryptedBytes))
                {
                    Save();
                }
            }
        }

        public bool AutoLogin
        {
            get { return GetValueOrDefault("AutoLogin", false); }
            set { if (AddOrUpdateValue("AutoLogin", value)) Save(); }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Update a setting value for our application. If the setting does not
        ///     exist, then add the setting.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected bool AddOrUpdateValue(string Key, Object value)
        {
            bool valueChanged = false;

            // If the key exists
            if (_settings.Contains(Key))
            {
                // If the value has changed
                if (_settings[Key] != value)
                {
                    // Store the new value
                    _settings[Key] = value;
                    valueChanged = true;
                }
            }
                // Otherwise create the key.
            else
            {
                _settings.Add(Key, value);
                valueChanged = true;
            }
            return valueChanged;
        }

        /// <summary>
        ///     Get the current value of the setting, or if it is not found, set the
        ///     setting to the default setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected T GetValueOrDefault<T>(string Key, T defaultValue)
        {
            T value;

            // If the key exists, retrieve the value.
            if (_settings.Contains(Key))
            {
                value = (T) _settings[Key];
            }
                // Otherwise, use the default value.
            else
            {
                value = defaultValue;
            }
            return value;
        }

        /// <summary>
        ///     Save the settings.
        /// </summary>
        protected void Save()
        {
            _settings.Save();
        }

        #endregion
    }
}