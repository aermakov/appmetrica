﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace AppMetrica.Behaviors
{
    public class UpdateSourceOnTextChangedBehavior : Behavior<Control>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            if (AssociatedObject is TextBox)
            {
                ((TextBox) AssociatedObject).TextChanged += OnTextChanged;
            }
        }

        protected override void OnDetaching()
        {
            if (AssociatedObject is TextBox)
            {
                ((TextBox) AssociatedObject).TextChanged -= OnTextChanged;
            }

            base.OnDetaching();
        }

        private void OnTextChanged(object sender, RoutedEventArgs routedEventArgs)
        {
            var textBox = (sender as TextBox);
            if (textBox == null) return;
            BindingExpression binding = textBox.GetBindingExpression(TextBox.TextProperty);
            if (binding != null)
            {
                binding.UpdateSource();
            }
        }
    }
}