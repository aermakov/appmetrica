﻿namespace AppMetrica.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly Settings _settings = new Settings();

        public string Login
        {
            get { return _settings.Login; }
            set { _settings.Login = value; }
        }

        public string Password
        {
            get { return _settings.Password; }
            set { _settings.Password = value; }
        }

        public bool AutoLogin
        {
            get { return _settings.AutoLogin; }
            set { _settings.AutoLogin = value; }
        }
    }
}