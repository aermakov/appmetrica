﻿using System;

namespace AppMetrica.Services
{
    public interface IAnalyticalService
    {
        void ReportEvent(string eventName);

        void ReportError(Exception ex);
    }
}