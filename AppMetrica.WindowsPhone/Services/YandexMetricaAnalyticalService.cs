﻿using System;
using Yandex.Metrica;

namespace AppMetrica.Services
{
    public class YandexMetricaAnalyticalService : IAnalyticalService
    {
        public void ReportEvent(string eventName)
        {
            Counter.ReportEvent(eventName);
        }

        public void ReportError(Exception ex)
        {
            Counter.ReportError(ex.Message, ex);
        }
    }
}