﻿namespace AppMetrica.Services
{
    public interface ISettingsService
    {
        string Login { get; set; }
        string Password { get; set; }
        bool AutoLogin { get; set; }
    }
}