﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Phone.Controls;

namespace AppMetrica.Controls
{
    public class TextBox : System.Windows.Controls.TextBox
    {
        public TextBox()
        {
            TextChanged += OnTextChanged;
        }

        protected void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            BindingExpression binding = GetBindingExpression(TextProperty);
            if (binding != null)
            {
                binding.UpdateSource();
            }
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);

            switch (e.Key)
            {
                case Key.Enter:
                {
                    var parent = Parent as FrameworkElement;
                    while (parent != null && !(parent is PhoneApplicationPage))
                    {
                        if (parent != null)
                        {
                            parent = parent.Parent as FrameworkElement;
                        }
                    }

                    if (parent != null)
                    {
                        ((PhoneApplicationPage) parent).Focus();
                    }
                }
                    break;
            }
        }
    }
}