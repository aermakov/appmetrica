﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using AppMetrica.Models;
using AppMetrica.Portable;
using AppMetrica.Portable.Attributes;
using AppMetrica.Portable.Enums;
using AppMetrica.Portable.Extensions;
using Caliburn.Micro;
using Newtonsoft.Json.Linq;
using Sparrow.Chart;

namespace AppMetrica.Controls
{
    public partial class ReportChart : INotifyPropertyChanged
    {
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Fields

        private BindableCollection<ChartPointModel> _categoryChartPoints;

        private BindableCollection<ChartPointModel> _dateTimeChartPoints;

        #endregion

        /// <summary>
        ///     The Report property
        /// </summary>
        public static readonly DependencyProperty ReportProperty =
            DependencyProperty.Register("Report", typeof (Report), typeof (ReportChart),
                new PropertyMetadata(null, OnReportChanged));

        /// <summary>
        ///     The ReportName property
        /// </summary>
        public static readonly DependencyProperty ReportNameProperty =
            DependencyProperty.Register("ReportName", typeof (string), typeof (ReportChart),
                new PropertyMetadata(null, OnReportNameChanged));

        public static readonly DependencyProperty XAxisIntervalProperty =
            DependencyProperty.Register("XAxisInterval", typeof (object), typeof (ReportChart),
                new PropertyMetadata(null, OnXAxisIntervalChanged));

        public ReportChart()
        {
            InitializeComponent();

            CategoryChartPoints = new BindableCollection<ChartPointModel>();
            CategoryListBox.DataContext = CategoryChartPoints;
            DateTimeChartPoints = new BindableCollection<ChartPointModel>();
            DateTimeChart.DataContext = DateTimeChartPoints;
        }

        /// <summary>
        ///     Gets or sets the interval.
        /// </summary>
        /// <value>
        ///     The interval.
        /// </value>
        public object XAxisInterval
        {
            get { return GetValue(XAxisIntervalProperty); }
            set { SetValue(XAxisIntervalProperty, value); }
        }

        public Report Report
        {
            get { return (Report) GetValue(ReportProperty); }
            set { SetValue(ReportProperty, value); }
        }

        public string ReportName
        {
            get { return (string) GetValue(ReportNameProperty); }
            set { SetValue(ReportNameProperty, value); }
        }

        public BindableCollection<ChartPointModel> CategoryChartPoints
        {
            get { return _categoryChartPoints; }
            set
            {
                if (Equals(value, _categoryChartPoints)) return;
                _categoryChartPoints = value;
                OnPropertyChanged();
            }
        }

        public BindableCollection<ChartPointModel> DateTimeChartPoints
        {
            get { return _dateTimeChartPoints; }
            set
            {
                if (Equals(value, _dateTimeChartPoints)) return;
                _dateTimeChartPoints = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Called when [interval changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnXAxisIntervalChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            ((ReportChart) sender).XAxisIntervalChanged(args);
        }

        private static void OnReportNameChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ReportChart) sender).ReportName = (string) e.NewValue;
        }

        /// <summary>
        ///     Intervals the changed.
        /// </summary>
        /// <param name="args">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        internal void XAxisIntervalChanged(DependencyPropertyChangedEventArgs args)
        {
            DateTimeChart.XAxis.Interval = (int) args.NewValue;
        }

        /// <summary>
        ///     Called when [Report changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnReportChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            ((ReportChart) sender).ReportChanged(args);
        }

        internal void ReportChanged(DependencyPropertyChangedEventArgs args)
        {
            var report = (Report) args.NewValue;

            ColumnHeader dimensionColumnHeader = report.DimensionColumnHeader;
            ColumnHeader metricColumnHeader = report.MetricColumnHeader;
            if (dimensionColumnHeader == null || metricColumnHeader == null) return;

            if (dimensionColumnHeader.IsDate)
            {
                DateTimeChart.Visibility = Visibility.Visible;
                CategoryGrid.Visibility = Visibility.Collapsed;

                AddPoints(DateTimeChartPoints, GetTimePoints(report));
            }
            else
            {
                CategoryGrid.Visibility = Visibility.Visible;
                DateTimeChart.Visibility = Visibility.Collapsed;

                AddPoints(CategoryChartPoints, GetCategoryPoints(report));

                TitleAttribute dimensionTitleAttribute = GetTitleAttribute(report.DimensionName);
                if (dimensionTitleAttribute != null)
                {
                    DimensionsTextBlock.Text = dimensionTitleAttribute.Title;
                }

                TitleAttribute metricTitleAttribute = GetTitleAttribute(report.MetricName);
                if (metricTitleAttribute != null)
                {
                    MetricTextBlock.Text = metricTitleAttribute.Title;
                }
            }
        }

        private static void AddPoints(BindableCollection<ChartPointModel> collection,
            IEnumerable<ChartPointModel> points)
        {
            collection.IsNotifying = false;
            collection.Clear();
            collection.AddRange(points);
            collection.IsNotifying = true;
            collection.Refresh();
        }

        private IEnumerable<ChartPointModel> GetTimePoints(Report report)
        {
            var points = new List<ChartPointModel>();
            int dimensionIndex = report.ColumnHeaders.IndexOf(report.DimensionColumnHeader);
            int metricIndex = report.ColumnHeaders.IndexOf(report.MetricColumnHeader);
            var days = (int) report.Query.EndDate.Subtract(report.Query.StartDate).TotalDays;
            for (int i = 0; i <= days; i++)
            {
                DateTime time = report.Query.StartDate.AddDays(i);
                string timeString = time.ToString("yyyy-MM-dd");
                int value = 0;
                List<object> objects = report.Rows.Find(x => Equals(x[dimensionIndex], timeString));
                if (objects != null && objects.Count > metricIndex)
                {
                    value = Convert.ToInt32(objects[metricIndex]);
                }

                var chartPoint =
                    new TimePoint
                    {
                        Time = time,
                        Value = value
                    };

                points.Add(new ChartPointModel(chartPoint));
            }

            return points;
        }

        private IEnumerable<ChartPointModel> GetCategoryPoints(Report report)
        {
            var points = new List<ChartPointModel>();
            int dimensionIndex = report.ColumnHeaders.IndexOf(report.DimensionColumnHeader);
            int metricIndex = report.ColumnHeaders.IndexOf(report.MetricColumnHeader);
            for (int i = 0; i < report.Rows.Count; i++)
            {
                List<object> objects = report.Rows[i];

                double value;
                object metric = objects[metricIndex];
                if (double.TryParse(metric.ToString(), out value) && value > 0)
                {
                    var chartPoint =
                        new CategoryPoint
                        {
                            Category = GetCategory(report.DimensionName, objects[dimensionIndex].ToString()),
                            Value = value
                        };

                    points.Add(new ChartPointModel(chartPoint));
                }
            }

            return points.OrderByDescending(x => x.Value);
        }

        private static string GetCategory(DimensionName dimensionName, string rawCategory)
        {
            string category;
            switch (dimensionName)
            {
                case DimensionName.CrashDumpObj:
                {
                    // {"value" : {"appVerHash" : "7370571860948403698","platformHash" : "10426962282362876278","crash" : ""},"id" : "11160318154034397263"},0
                    JObject jObject = JObject.Parse(rawCategory);

                    category = jObject.SelectToken("value.crash").Value<string>();
                }
                    break;
                case DimensionName.RegionCityObj:
                {
                    JObject jObject = JObject.Parse(rawCategory);

                    // {"value" : "","id" : "0"}
                    category = jObject.Value<string>("value");
                }
                    break;
                case DimensionName.RegionCountryObj:
                {
                    JObject jObject = JObject.Parse(rawCategory);

                    // {"value" : "","id" : "0"}
                    category = jObject.Value<string>("value");
                }
                    break;
                case DimensionName.ScreenResolutionObj:
                {
                    //{"value" : {"w" : "480","h" : "800"},"id" : "480 800"},2
                    JObject jObject = JObject.Parse(rawCategory);

                    var width = jObject.SelectToken("value.w").Value<int>();
                    var height = jObject.SelectToken("value.h").Value<int>();
                    category = string.Format("{0}x{1}", width, height);
                }
                    break;
                case DimensionName.SessionTimeIntervalObj:
                {
                    // {"value" : {"to" : 9,"from" : 1},"id" : 1},3
                    JObject jObject = JObject.Parse(rawCategory);

                    string fromTimeSuffix;
                    string toTimeSuffix;
                    var from = jObject.SelectToken("value.from").Value<int>();
                    var to = jObject.SelectToken("value.to").Value<int>();
                    string fromTimeString = TimeSpan.FromSeconds(from).ToTimeString(out fromTimeSuffix);
                    string toTimeString = TimeSpan.FromSeconds(to).ToTimeString(out toTimeSuffix);
                    if (from == to || fromTimeString.Equals(toTimeString))
                    {
                        category = string.Format("{0} {1}", fromTimeString, fromTimeSuffix);
                    }
                    else
                    {
                        category = string.Format("{0}-{1} {2}", fromTimeString, toTimeString, toTimeSuffix);
                    }
                }
                    break;
                default:
                    category = rawCategory;
                    break;
            }

            if (string.IsNullOrEmpty(category))
            {
                category = "Не определено";
            }
            return category;
        }

        private TitleAttribute GetTitleAttribute<T>(T t)
        {
            MemberInfo memberInfo = typeof (T).GetMember(t.ToString()).FirstOrDefault();
            TitleAttribute attribute = null;
            if (memberInfo != null)
            {
                attribute = (TitleAttribute) memberInfo.GetCustomAttributes(typeof (TitleAttribute), false)
                    .FirstOrDefault();
            }

            return attribute;
        }
    }
}